package com.example.logintest.model

/*class Orders(name: String?, nameClient: String?, quantity: Int?, price: Double?, image: Int?) {
    var nameOrder:String? = name;
    var nameClientOrder:String?=nameClient
    var quantityOrder:Int=quantity
    var priceOrder=price
    var imageOrder=image
}*/
class Orders {
    var nameOrder:String? = null;
    var nameClientOrder:String?=null
    var quantityOrder:Int=0
    var priceOrder:Double=0.00
    var imageOrder:Int=0
    var totalPrice:Double=0.00
    var orderId:String?=null
}