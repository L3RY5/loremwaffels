package com.example.logintest

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import kotlinx.android.synthetic.main.activity_main.*


/*
Sedric Yaovi Lodonou
Programmeren 5
KoekieApp : Lorem Waffle
*/

class MainActivity : AppCompatActivity() {
    val palinMap: MutableMap<String, String> = mutableMapOf();
    private lateinit var auth: FirebaseAuth
    val RC_SIGN_IN: Int = 1
    lateinit var mGoogleSignInClient: GoogleSignInClient
    lateinit var mGoogleSignInOptions: GoogleSignInOptions


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        auth = FirebaseAuth.getInstance()

        configureGoogleSignIn()
        setupUI()
    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val user = auth.currentUser
        if (user != null) {
            println(" ${user.metadata} gebruiker al ingelogt")
            startActivity(ListWaffels.getLaunchIntent(this))
            finish()
        }
    }

    //Sign in with google*********************************
    // Configure Google Sign In
    private fun configureGoogleSignIn() {
        mGoogleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, mGoogleSignInOptions)
    }

    private fun setupUI() {
        googleSignBtn.setOnClickListener {
            signInWithGoofle()
        }
    }

    private fun signInWithGoofle() {
        val signInIntent: Intent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                firebaseAuthWithGoogle(account)
            } catch (e: ApiException) {
                Toast.makeText(this, "Google sign in failed:(", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount?) {
        val credential = GoogleAuthProvider.getCredential(acct!!.idToken, null)
        auth.signInWithCredential(credential).addOnCompleteListener {
            if (it.isSuccessful) {
                Toast.makeText(
                    baseContext, "Welcome  ${acct.displayName}.",
                    Toast.LENGTH_LONG
                ).show()
                startActivity(ListWaffels.getLaunchIntent(this))
            } else {
                Toast.makeText(this, "Google sign in failed:(", Toast.LENGTH_LONG).show()
            }
        }
    }

    //End Sign in with google*********************************
    //sign in with email-pass
    fun goToListWaffles(view: View) {

        var userN: String = usernameLog.text.toString();
        var passW: String = passwordLog.text.toString();
        if (userN.trim().isNullOrEmpty()) {
            val saveAlert = AlertDialog.Builder(this)
            saveAlert.setTitle("username  Error")
            saveAlert.setMessage("The username filed can't be empty")
            saveAlert.setPositiveButton("OK") { dialogInterface: DialogInterface, i: Int ->
                dialogInterface.dismiss()
            }
            saveAlert.show()
        } else if (passW.trim().isNullOrEmpty()) {
            val saveAlert = AlertDialog.Builder(this)
            saveAlert.setTitle("password Error")
            saveAlert.setMessage("The password filed can't be empty")
            saveAlert.setPositiveButton("OK") { dialogInterface: DialogInterface, i: Int ->
                dialogInterface.dismiss()
            }
            saveAlert.show()
        } else {
            // [START sign_in_with_email]
            auth.signInWithEmailAndPassword(userN, passW)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithEmail:success")
                        val user = auth.currentUser
                        println(user)
                        Toast.makeText(
                            baseContext, "Welcome  ${user!!.email}",
                            Toast.LENGTH_LONG
                        ).show()

                        val intent = Intent(this, ListWaffels::class.java)
                        intent.putExtra("hash", palinMap.toString())
                        intent.action = Intent.ACTION_SEND
                        startActivity(intent)
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInWithEmail:failure", task.exception)
                        Toast.makeText(
                            baseContext, "Authentication failed.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

        }
        // [END sign_in_with_email]
    }

    fun goToRegister(view: View) {
        val intent = Intent(this, Register::class.java)
        intent.action = Intent.ACTION_SEND
        startActivity(intent)
    }

    companion object {
        private const val TAG = "EmailPassword"

        fun getLaunchIntent(from: Context) = Intent(from, MainActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }
    }
}
