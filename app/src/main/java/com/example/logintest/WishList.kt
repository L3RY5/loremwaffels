package com.example.logintest

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.example.logintest.data.WishlistAdapter
import com.example.logintest.model.Orders
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_wish_list.*
import kotlinx.android.synthetic.main.wishlist_card_view.*
import java.text.DecimalFormat

/*
Sedric Yaovi Lodonou
Programmeren 5
KoekieApp : Lorem Waffle
*/

class WishList : AppCompatActivity() {

    private var adapter: WishlistAdapter? = null
    private var mealListCrds: ArrayList<Orders>? = null
    private var layoutManager: RecyclerView.LayoutManager? = null
    // Access a Cloud Firestore instance from your Activity
    val db = FirebaseFirestore.getInstance()
    private lateinit var auth: FirebaseAuth
    private val df = DecimalFormat("0.00")
    val ordersRef = db.collection("orders")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wish_list)
        auth = FirebaseAuth.getInstance()

        var imageIdList = arrayOf<Int>(
            R.drawable.brusselswaffel,
            R.drawable.brusselswafflenuttela,
            R.drawable.liegewaffle,
            R.drawable.pizzelle,
            R.drawable.americanwaffle,
            R.drawable.scandinavianwaffles,
            R.drawable.potatowaffle
        )

        var typeWaffelsList: Array<String> = arrayOf(
            "Brussels waffle",
            "Brussels waffle nutella",
            "Liège waffle",
            "Pizzelle",
            "American waffle",
            "Scandinavian waffles",
            "Potato waffle"
        )

        var prijsWaffels: Array<String> = arrayOf(
            "2.30", "2.50", "2.00", "3.00", "2.90", "3.50", "3.20"
        )

        val user = auth.currentUser
        val docRef = db.collection("Orders").document("${user!!.uid}").collection("PersonalOrders")
        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    var totalPayment: ArrayList<Double> = arrayListOf<Double>()
                    mealListCrds = ArrayList<Orders>()
                    layoutManager = LinearLayoutManager(this)
                    adapter = WishlistAdapter(mealListCrds!!, this)
                    recyclerViewSelected2.layoutManager = layoutManager
                    recyclerViewSelected2.adapter = adapter
                    for (d in document) {

                        var itemNr2: Int = typeWaffelsList.indexOf("${d.data["type"]}")

                        var orders = Orders()
                        orders.imageOrder = imageIdList[itemNr2]
                        orders.nameClientOrder = d.get("firstname").toString()
                        orders.nameOrder = d.get("type").toString()
                        orders.priceOrder = prijsWaffels[itemNr2].toDouble()
                        val test: Int = "${d.data["quantity"]}".toInt()
                        orders.quantityOrder = test
                        orders.totalPrice = Math.round(prijsWaffels[itemNr2].toDouble() * test).toDouble()

                        var totalTopay = Math.round((prijsWaffels[itemNr2].toDouble() * test.toDouble()) * 100) / 100
                        orders.orderId = d.id
                        totalPayment.add(totalTopay.toDouble())
                        mealListCrds?.add(orders)
                    }
                    adapter!!.notifyDataSetChanged()
                    recyclerViewSelected2.setOnClickListener { }
                    println(totalPayment.sum())
                    totalCostOrders.setText("Total order cost : ${totalPayment.sum()} Euro")
                } else {

                    println("No such document")
                }
            }
            .addOnFailureListener { exception ->
                println(exception)
            }
    }

    fun deleteOrder(view: View) {
        println(lv_orderId.text.toString())
        val user = auth.currentUser
        val docRef = db.collection("ordersTest").document("${user!!.uid}").collection("PersonalOrders")
            .document(lv_orderId.text.toString())
        docRef.delete()
            .addOnSuccessListener {
                finish();
                overridePendingTransition(0, 0);
                startActivity(getIntent());
                overridePendingTransition(0, 0);
            }
            .addOnFailureListener { }
    }
}



