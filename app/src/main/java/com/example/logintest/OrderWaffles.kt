package com.example.logintest

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.support.annotation.RequiresApi
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_order_waffles.*
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.*
import kotlin.collections.HashMap

/*
Sedric Yaovi Lodonou
Programmeren 5
KoekieApp : Lorem Waffle
*/

class OrderWaffles : AppCompatActivity() {
    // Access a Cloud Firestore instance from your Activity
    val db = FirebaseFirestore.getInstance()
    val PICK = 2015
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_waffles)
        auth = FirebaseAuth.getInstance()

        val waffels = arrayOf(
            "Brussels waffle",
            "Brussels waffle nutella",
            "Liège waffle",
            "Pizzelle",
            "American waffle",
            "Scandinavian waffles",
            "Potato waffle"
        )
        // Set the number picker minimum and maximum value
        number_picker.minValue = 0
        number_picker.maxValue = 10
        // Set the number picker minimum and maximum values
        number_picker_kind.minValue = 0
        number_picker_kind.maxValue = waffels.size - 1

        number_picker.wrapSelectorWheel = false
        number_picker_kind.wrapSelectorWheel = false

        // Set the valued to be displayed in number picker
        number_picker_kind.displayedValues = waffels

        // Set number picker value change listener
        number_picker_kind.setOnValueChangedListener { _, _, newVal ->

            // Display the picker selected value to text view
            kinfWafflesDisplay.text = " ${waffels[newVal]}"
        }

        // Set number picker value changed listener
        number_picker.setOnValueChangedListener { picker, oldVal, newVal ->

            //Display the newly selected number to text view
            quantityDisplay.text = " $newVal"
        }

        contactList.setOnClickListener { view ->
            var intent = Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI)
            startActivityForResult(intent, PICK)
        }
        setupUI()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK && resultCode == Activity.RESULT_OK) {
            var contact: Uri = data!!.data
            var cursor: Cursor = contentResolver.query(contact, null, null, null)
            cursor.moveToFirst()
            var contactPhone = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
            var contactName = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)

            var phoneS = cursor.getString(contactPhone)
            var nameS = cursor.getString(contactName)


            name.setText("${nameS}")
            telNr.setText("${phoneS}")
        }
    }

    private fun setupUI() {
        signOutOrdersPage.setOnClickListener {
            signOut()
        }
    }

    private fun signOut() {
        startActivity(MainActivity.getLaunchIntent(this))
        FirebaseAuth.getInstance().signOut()
    }

    fun orderMain(view: View) {
        if (!kinfWafflesDisplay.text.trim().isNullOrEmpty() && !quantityDisplay.text.trim().isNullOrEmpty()) {
            if (!name.text.trim().toString().isNullOrEmpty() && !lastName.text.trim().toString().isNullOrEmpty() && !telNr.text.trim().toString().isNullOrEmpty()) {
                val sdf = SimpleDateFormat("dd/M/yyyy hh:mm:ss")
                val currentDate = sdf.format(Date())
                val user = auth.currentUser
                val ordersRef = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    db.collection("Orders").document("${user!!.uid}").collection("PersonalOrders").document(
                        LocalDateTime.now().toString()
                    )
                } else {
                    TODO("VERSION.SDK_INT < O")
                }
                //creat a new doc in collection orders
                val order = HashMap<String, Any>()
                var firstN = name.text.trim().toString()
                order["firstname"] = name.text.trim().toString().toLowerCase()
                order["lastname"] = lastName.text.trim().toString().toLowerCase()
                order["quantity"] = quantityDisplay.text.trim()
                order["tel"] = telNr.text.trim().toString()
                order["type"] = kinfWafflesDisplay.text.trim().toString()

                //add de document
                ordersRef.set(order).addOnSuccessListener { documentReference ->
                    Toast.makeText(
                        baseContext, "order received " + firstN + ".",
                        Toast.LENGTH_SHORT
                    ).show()

                    name.setText("")
                    lastName.setText("")
                    quantityDisplay.text = ""
                    telNr.setText("")
                    kinfWafflesDisplay.text = ""
                }
                    .addOnFailureListener { e ->

                        Toast.makeText(
                            baseContext, "Error receiving order.",
                            Toast.LENGTH_LONG
                        ).show()
                    }
            } else {
                val saveAlert = AlertDialog.Builder(this)
                saveAlert.setTitle("Order Error")
                saveAlert.setMessage("Pleas fill in all personal information for this order")
                saveAlert.setPositiveButton("OK") { dialogInterface: DialogInterface, i: Int ->
                    dialogInterface.dismiss()
                }
                saveAlert.show()
            }

        } else {
            val saveAlert = AlertDialog.Builder(this)
            saveAlert.setTitle("Order Error")
            saveAlert.setMessage("Choose waffle type and quantity")
            saveAlert.setPositiveButton("OK") { dialogInterface: DialogInterface, i: Int ->
                dialogInterface.dismiss()
            }
            saveAlert.show()
        }
    }

    fun goToWishList(view: View) {
        val intent = Intent(this, WishList::class.java)
        intent.action = Intent.ACTION_SEND
        startActivity(intent)
    }


}







