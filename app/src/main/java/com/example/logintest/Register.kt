package com.example.logintest

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_register.*

/*
Sedric Yaovi Lodonou
Programmeren 5
KoekieApp : Lorem Waffle
*/

class Register : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        intent.getSerializableExtra("hash")

// ...
// Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()
    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser

    }

    fun registerUser(view: View) {
        val regEmai = usernameRegister.text.trim().toString()
        val regPass1 = passwordRegister.text.trim().toString()
        val regPass2 = passwordRegister2.text.trim().toString()

        if (!TextUtils.isEmpty(regEmai) &&
            !TextUtils.isEmpty(regPass1) &&
            !TextUtils.isEmpty(regPass2)
        ) {
            if (regPass1.equals(regPass2)) {
                auth.createUserWithEmailAndPassword(regEmai, regPass1)
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {

                            val user = auth.currentUser
                            val intent = Intent(this, MainActivity::class.java)
                            // intent.putExtra("hash", palinMap.toString())
                            intent.action = Intent.ACTION_SEND
                            startActivity(intent)

                            println(user?.displayName)
                        } else {
                            // If sign in fails, display a message to the user.

                            Toast.makeText(
                                baseContext, task.getException()!!.message,
                                Toast.LENGTH_SHORT
                            ).show()

                        }

                    }
            } else {
                val saveAlert = AlertDialog.Builder(this)
                saveAlert.setTitle("Password  mismatch")
                saveAlert.setMessage("Enter same passwords in both fields ")
                saveAlert.setPositiveButton("OK") { dialogInterface: DialogInterface, i: Int ->
                    dialogInterface.dismiss()
                }
                saveAlert.show()
            }

        } else {
            Toast.makeText(
                baseContext, "Field(s) can't be ampty",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    fun sendPass(view: View) {
        val email = usernameRegister.text.trim().toString()
        if (!TextUtils.isEmpty(email)) {
            auth!!.sendPasswordResetEmail(email).addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val message = "email  with password is send to ${email}"
                    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

                } else {
                    Toast.makeText(this, "No user found with this email.", Toast.LENGTH_LONG).show()
                }
            }
        }
    }
}
