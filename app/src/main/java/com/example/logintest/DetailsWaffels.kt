package com.example.logintest

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_details_waffels.*

/*
Sedric Yaovi Lodonou
Programmeren 5
KoekieApp : Lorem Waffle
*/
class DetailsWaffels : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details_waffels)


        countryDetail.text = intent.getStringExtra("country")
        naamDetail.text = intent.getStringExtra("name")
        priceDetial.text = intent.getStringExtra("price")
        extraDetial.text = intent.getStringExtra("info")
        imageDetail.setImageResource(intent.getIntExtra("image", DEFAULT_BUFFER_SIZE))


    }
}
