package com.example.logintest

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.example.logintest.data.WafflesAdapter
import com.example.logintest.model.Waffels
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_list_waffels.*

/*
Sedric Yaovi Lodonou
Programmeren 5
KoekieApp : Lorem Waffle
*/
class ListWaffels : AppCompatActivity() {

    private var adapter: WafflesAdapter? = null
    private var mealListCrds: ArrayList<Waffels>? = null
    private var layoutManager: RecyclerView.LayoutManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_waffels)

        mealListCrds = ArrayList<Waffels>()
        layoutManager = LinearLayoutManager(this)
        adapter = WafflesAdapter(mealListCrds!!, this)
        recyclerViewSelected.layoutManager = layoutManager
        recyclerViewSelected.adapter = adapter

        var imageIdList = arrayOf<Int>(
            R.drawable.brusselswaffel,
            R.drawable.brusselswafflenuttela,
            R.drawable.liegewaffle,
            R.drawable.pizzelle,
            R.drawable.americanwaffle,
            R.drawable.scandinavianwaffles,
            R.drawable.potatowaffle
        )

        var typeWaffelsList: Array<String> = arrayOf(
            "Brussels waffle",
            "Brussels waffle nutella",
            "Liège waffle",
            "Pizzelle",
            "American waffle",
            "Scandinavian waffles",
            "Potato waffle"
        )

        var prijsWaffels: Array<String> = arrayOf(
            "2.30", "2.50", "2.00", "3.00", "2.90", "3.50", "3.20"
        )

        var infoWaffels: Array<String> = arrayOf(
            "Brussels Waffle is the best waffle that you can find in Brussels, in every bistro, treinstation. it is always hot and delicious",
            "Brussels Waffle is the best waffle that you can find in Brussels, in every bistro, treinstation. it is always hot and delicious. with nutella",
            "Lieges Waffle is the best waffle that you can find in Liege, in every bistro, treinstation. it is always hot and delicious",
            "Pizzelle is the best waffle that you can find in Italy, in every bistro, treinstation. it is always hot and delicious",
            "American waffle is the best waffle that you can find in the USA, in every bistro, treinstation. it is always hot and delicious",
            "Scandinavian waffles is the best waffle that you can find in the Nordic countries, in every bistro, treinstation. it is always hot and delicious",
            "Potato waffle is the best waffle that you can find in the UK, in every bistro, treinstation. it is always hot and delicious"
        )

        var CountryWaffelsList: Array<String> = arrayOf(
            "Belguim",
            "Belguim",
            "Belguim",
            "Italia",
            "USA",
            "Nordic countries",
            "UK"
        )

        for (i in 0..6) {
            var waffel = Waffels()
            waffel.typeWaffles = typeWaffelsList[i]
            waffel.priceWaffles = prijsWaffels[i]
            waffel.extraInfoWaffles = infoWaffels[i]
            waffel.img = imageIdList[i]
            waffel.originWaffles = CountryWaffelsList[i]
            mealListCrds?.add(waffel)
        }
        adapter!!.notifyDataSetChanged()
        recyclerViewSelected.setOnClickListener { }
        setupUI()
    }

    private fun setupUI() {
        googleSignOutBtn.setOnClickListener {
            signOut()
        }
    }

    private fun signOut() {
        startActivity(MainActivity.getLaunchIntent(this))
        FirebaseAuth.getInstance().signOut();
    }

    fun goToOrderPage(view: View) {
        val intent = Intent(view.context, OrderWaffles::class.java)
        intent.action = Intent.ACTION_SEND
        startActivity(intent)
    }

    companion object {
        fun getLaunchIntent(from: Context) = Intent(from, ListWaffels::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }
    }
}
