package com.example.logintest.data

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.logintest.R
import com.example.logintest.model.Orders

/*
Sedric Yaovi Lodonou
Programmeren 5
KoekieApp : Lorem Waffle
*/

    class WishlistAdapter(private val list: ArrayList<Orders>, private val context: Context) :
        RecyclerView.Adapter<WishlistAdapter.ViewHolder>() {

        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            fun bindItem(orders: Orders) {
                var waffelName: TextView = itemView.findViewById(R.id.lv_nameWaffles) as TextView
                var clientName: TextView = itemView.findViewById(R.id.lv_client) as TextView
                var quantity: TextView = itemView.findViewById(R.id.lv_quantity) as TextView
                var totalPrice: TextView = itemView.findViewById(R.id.lv_totalPrice) as TextView
                var wafflePrice: TextView = itemView.findViewById(R.id.lv_price) as TextView
                var orderId: TextView = itemView.findViewById(R.id.lv_orderId) as TextView
                var imageofWaffel: ImageView = itemView.findViewById(R.id.waffleImageOrder) as ImageView
                waffelName.text = orders.nameOrder
                clientName.text = orders.nameClientOrder
                quantity.text = orders.quantityOrder.toString()+ " Pieces"
                totalPrice.text=orders.totalPrice.toString()+ " Total"
                wafflePrice.text=orders.priceOrder.toString()+ " Euro"
                orderId.text=orders.orderId.toString()
                //imagesOfMeal.im=meal.img
                imageofWaffel.setImageResource(orders.imageOrder)


            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): WishlistAdapter.ViewHolder {
            val view = LayoutInflater.from(context).inflate(R.layout.wishlist_card_view, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onBindViewHolder(holder: WishlistAdapter.ViewHolder, position: Int) {
            holder.bindItem(list[position])
        }


    }