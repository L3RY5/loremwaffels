package com.example.logintest.data

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.logintest.DetailsWaffels
import com.example.logintest.R
import com.example.logintest.model.Waffels

/*
Sedric Yaovi Lodonou
Programmeren 5
KoekieApp : Lorem Waffle
*/
class WafflesAdapter(private val list: ArrayList<Waffels>, private val context: Context) :
    RecyclerView.Adapter<WafflesAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItem(waffels: Waffels) {
            var waffelName: TextView = itemView.findViewById(R.id.lv_dishe) as TextView
            var countryOfOrigin: TextView = itemView.findViewById(R.id.lv_countryOfOrigin) as TextView
            var prijsWaffels: TextView = itemView.findViewById(R.id.lv_price) as TextView
            var extraInformation: TextView = itemView.findViewById(R.id.lv_info) as TextView
            var imageofWaffel: ImageView = itemView.findViewById(R.id.waffleImage) as ImageView
            waffelName.text = waffels.typeWaffles
            countryOfOrigin.text = waffels.originWaffles
            prijsWaffels.text = waffels.priceWaffles
            extraInformation.text=waffels.extraInfoWaffles
            //imagesOfMeal.im=meal.img
            imageofWaffel.setImageResource(waffels.img)

            itemView.setOnClickListener{
                val intent = Intent(context, DetailsWaffels::class.java)
                intent.putExtra("name",waffels.typeWaffles.toString())
                intent.putExtra("country",waffels.originWaffles.toString())
                intent.putExtra("price",waffels.priceWaffles.toString())
                intent.putExtra("info",waffels.extraInfoWaffles.toString())
                intent.putExtra("image", waffels.img!!)
                context.startActivity(intent)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): WafflesAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.card_view, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: WafflesAdapter.ViewHolder, position: Int) {
        holder.bindItem(list[position])
    }


}